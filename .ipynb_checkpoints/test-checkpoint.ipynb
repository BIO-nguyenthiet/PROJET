{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Projet phylogénétique\n",
    "Théophile Sanchez (theophile.sanchez@inria.fr) - Sarah Cohen Boulakia\n",
    "\n",
    "------\n",
    "\n",
    "## Introduction\n",
    "\n",
    "Au cours de ce projet, vous étudierez trois espèces disparues de félins qui vivaient autrefois sur le continent Américain. Ces trois espèces, le _smilodon_ (tigre à dents de sabre), l'_homotherium_ (_scimitar toothed tigers_) et _M. trumani_ (guépard américain) se sont éteintes il y a environ 13 000 ans, à la fin de la dernière période glaciaire. Des séquences ADN partielles de la protéine cytochrome b de ces espèces ont pu être séquencées et vont vous permettre de retrouver les liens de parentés entre ces espèces et des espèces de félins contemporaines : le chat domestique, le lion, le léopard, le tigre, le puma, le guépard et les chats sauvages africains, chinois et européens. Sont aussi présent dans le jeu de donnée des séquences issues d'espèces extérieures aux félins.\n",
    "\n",
    "Afin de reconstruire l'arbre phylogénétique de ces espèces, vous utiliserez une méthode basée sur le calcul des distances évolutives entre les séquences ADN des protéines. Sachez qu'une démarche similaire peut-être appliquée aux séquences d'acides aminés.\n",
    "\n",
    "Les différentes étapes qui vous permetterons de construire l'arbre sont détaillées dans ce notebook. Vous devrez implémenter les algorithmes en Python et répondre aux questions dans les cellules prévues.\n",
    "\n",
    "Quelques conseils :\n",
    "- Utiliser au maximum les fonctions présentes dans les packages de python (sauf si il vous est explicitement demandé de les réimplémenter). Si un problème vous paraît courant, il existe surement déjà une fonction pour le résoudre. Pour ce projet vous serez limité aux packages de base, à Numpy et ETE (seulement pour l'affichage des arbres).\n",
    "- Si une partie de votre code ne vous semble pas très explicite, ajoutez des commentaires pour l'expliquer. Une personne qui lit votre code doit pouvoir comprendre son fonctionnement facilement.\n",
    "- N'hésitez pas à chercher dans la documentation et sur internet. Cependant, faites attention au plagiat !\n",
    "\n",
    "Le projet est à rendre **en binôme** par mail. Vous regrouperez votre notebook et les fichiers nécessaires à son fonctionnement dans une archive portant vos noms et prénoms."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "------\n",
    "## Importation des séquences\n",
    "\n",
    "Le format FASTA permet de stocker plusieurs séquences (ADN, ARN ou peptidiques) dans un fichier. Les séquences que vous allez étudier ont été regroupées dans le fichier `cat_dna.fasta`.\n",
    "\n",
    "**Exercice 1 :** Écriver une fonction permettant d'importer un fichier au format fasta et de le stocker dans un dictionnaire. Les clés seront les noms des séquences et les valeurs du dictionnaire seront les séquences d'adn."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# À remplir"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "------\n",
    "## Alignement des séquences\n",
    "\n",
    "La méthode que vous utiliserez pour calculer l'arbre phylogénétique nécessite de calculer la distance évolutive entre les séquences. Avant de pouvoir les calculer, il faut d'abord aligner les séquences en considérant trois types de mutations :\n",
    "- les substitutions (un nucléotide est remplacé par un autre)\n",
    "- les insertions\n",
    "- les délétions\n",
    "Par exemple, les séquences \"ACTCCTGA\" et \"ATCTCGTGA\" ont plusieurs alignements possibles : \n",
    "\n",
    "$A_1$ :\n",
    "```\n",
    "-ACTCCTGA\n",
    "ATCTCGTGA\n",
    "```\n",
    "\n",
    "$A_2$ :\n",
    "```\n",
    "A-CTCCTGA\n",
    "ATCTCGTGA\n",
    "```\n",
    "\n",
    "$A_3$ :\n",
    "```\n",
    "AC-TCCTGA\n",
    "ATCTCGTGA\n",
    "```\n",
    ".\n",
    "\n",
    ".\n",
    "\n",
    ".\n",
    "\n",
    "Le \"-\" désigne un *gap*, c'est à dire un \"trou\" dans l'alignement qui a été causé par une insertion ou une déletion. On regroupe ces deux types de mutations sous le terme indel.\n",
    "\n",
    "Ces alignements correspondent à une multitude d'histoires phylogénétiques différentes. Pour sélectionner le meilleur alignement il faut donc introduire l'hypothèse du maximum de parcimonie qui privilégie l'histoire phylogénétique qui implique le moins d'hypothèses et donc, le moins de changements évolutifs. Par exemple, parmis les trois alignements ci-dessus on preferera l'alignement 2 car il correspond au scénario avec le moins de mutations:\n",
    "- l'alignement 1 implique au minimum 1 indel et 2 substitutions\n",
    "- l'alignement 2 implique au minimum 1 indel et 1 substitutions\n",
    "- l'alignement 3 implique au minimum 1 indel et 2 substitutions\n",
    "\n",
    "On peut maintenant définir un score d'identité que l'on va augmenter de 1 lorsque qu'il n'y pas eu de mutation et ainsi obtenir la matrice suivante :\n",
    "\n",
    "|   &nbsp;   | A | C | G | T | - |\n",
    "|   -   | - | - | - | - | - |\n",
    "| **A** | 1 | 0 | 0 | 0 | 0 |\n",
    "| **C** | 0 | 1 | 0 | 0 | 0 |\n",
    "| **G** | 0 | 0 | 1 | 0 | 0 |\n",
    "| **T** | 0 | 0 | 0 | 1 | 0 |\n",
    "| **-** | 0 | 0 | 0 | 0 | 0 |\n",
    "\n",
    "Cette matrice correspond au modèle d'évolution de l'ADN défini par Jukes et Cantor qui fait l'hypothèse d'un taux de mutation équivalent pour chacun des nucléotides. Cependant, en réalité ces taux ne sont pas les mêmes partout, on sait par exemple que le taux de transition (substitution A$\\leftrightarrow$G ou C$\\leftrightarrow$T) est différent du taux de transversions (substitution A$\\leftrightarrow$T, C$\\leftrightarrow$G, C$\\leftrightarrow$A ou G$\\leftrightarrow$T) et que d'autres facteurs devrait être pris en compte comme la fréquence du nucléotide dans l'ADN. [C'est pour cette raison qu'il existe beaucoup de modèles différents d'écrivant l'évolution de l'ADN.](https://en.wikipedia.org/wiki/Models_of_DNA_evolution) Dans la suite de ce projet nous utiliserons la matrice de similarité $S$ suivante : \n",
    "\n",
    "|   &nbsp;   | A  | C  | G  | T  | -  |\n",
    "|   -   | -  | -  | -  | -  | -  |\n",
    "| **A** | 10 | -1 | -3 | -4 | -5 |\n",
    "| **C** | -1 | 7  | -5 | -3 | -5 |\n",
    "| **G** | -3 | -5 | 9  | 0  | -5 |\n",
    "| **T** | -4 | -3 | 0  | 8  | -5 |\n",
    "| **-** | -5 | -5 | -5 | -5 | -5 |\n",
    "\n",
    "**Exercice 2 :** Écriver la fonction permettant de calculer le score entre deux alignements avec la matrice de similarité précédente puis afficher le score des trois alignements $A_1$, $A_2$ et $A_3$. La classe permettant d'importer une matrice et de calculer le score entre deux lettres vous est déjà fournie, la matrice de similarité est stockée dans le fichier `dna_matrix` :\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import numpy as np\n",
    "\n",
    "\n",
    "class SimilarityMatrix:\n",
    "    def __init__(self, filename):\n",
    "        with open(filename) as f:\n",
    "            self.letters = f.readline().split()\n",
    "            self.values = np.loadtxt(filename, skiprows=1, usecols=range(1, len(self.letters) + 1))\n",
    "        \n",
    "    def score(self, letter1, letter2): # return the similarity score between letter1 and letter2\n",
    "        return self.values[self.letters.index(letter1)][self.letters.index(letter2)]\n",
    "    \n",
    "# Example\n",
    "similarity_matrix = SimilarityMatrix('dna_matrix')\n",
    "print('Score between G and C:', similarity_matrix.score('G', 'C'))\n",
    "print('Score between A and a gap:', similarity_matrix.score('-', 'A'))"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# À remplir"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "------\n",
    "### Algorithme de Needleman-Wunsch\n",
    "\n",
    "Maintenant que vous avez vu ce qu'est une matrice de similarité et comment calculer le score de similarité d'un alignement, vous allez devoir implémenter un algorithme permettant de trouver le meilleur alignement global entre deux séquences. Avec deux séquences à aligner de taille $n$ et $m$, la première étape consiste à initialiser deux matrices de taille $(n+1) \\times (m+1)$. La première est la matrice de score $M$ et la seconde sera la matrice de *traceback* $T$. \n",
    "\n",
    "Par exemple, avec la matrice $S$ et les séquences $A =$ \"ACTCCTGA\" et $B =$ \"ATCTCGTGA\", on initialise $M$ comme si l'on ajoutait des *gaps* partout :\n",
    "\n",
    "|   &nbsp;   | - | A | T | C | T | C | G | T | G | A |\n",
    "|   -   | - | - | - | - | - | - | - | - | - | - |\n",
    "| **-** | 0 |-5 |-10|-15|-20|-25|-30|-35|-40|-45|\n",
    "| **A** |-5 | &nbsp; | &nbsp; | &nbsp; | &nbsp; | &nbsp; | &nbsp; | &nbsp; | &nbsp; | &nbsp; |\n",
    "| **C** |-10| &nbsp; | &nbsp; | &nbsp; | &nbsp; | &nbsp; | &nbsp; | &nbsp; | &nbsp; | &nbsp; |\n",
    "| **T** |-15| &nbsp; | &nbsp; | &nbsp; | &nbsp; | &nbsp; | &nbsp; | &nbsp; | &nbsp; | &nbsp; |\n",
    "| **C** |-20| &nbsp; | &nbsp; | &nbsp; | &nbsp; | &nbsp; | &nbsp; | &nbsp; | &nbsp; | &nbsp; |\n",
    "| **C** |-25| &nbsp; | &nbsp; | &nbsp; | &nbsp; | &nbsp; | &nbsp; | &nbsp; | &nbsp; | &nbsp; |\n",
    "| **T** |-30| &nbsp; | &nbsp; | &nbsp; | &nbsp; | &nbsp; | &nbsp; | &nbsp; | &nbsp; | &nbsp; |\n",
    "| **G** |-45| &nbsp; | &nbsp; | &nbsp; | &nbsp; | &nbsp; | &nbsp; | &nbsp; | &nbsp; | &nbsp; |\n",
    "| **A** |-40| &nbsp; | &nbsp; | &nbsp; | &nbsp; | &nbsp; | &nbsp; | &nbsp; | &nbsp; | &nbsp; ||\n",
    "\n",
    "Puis on initialise $T$ :\n",
    "\n",
    "|   &nbsp;   | - | A | T | C | T | C | G | T | G | A |\n",
    "|   -   | - | - | - | - | - | - | - | - | - | - |\n",
    "| **-** | o | l | l | l | l | l | l | l | l | l |\n",
    "| **A** | u | &nbsp; | &nbsp; | &nbsp; | &nbsp; | &nbsp; | &nbsp; | &nbsp; | &nbsp; | &nbsp; |\n",
    "| **C** | u | &nbsp; | &nbsp; | &nbsp; | &nbsp; | &nbsp; | &nbsp; | &nbsp; | &nbsp; | &nbsp; |\n",
    "| **T** | u | &nbsp; | &nbsp; | &nbsp; | &nbsp; | &nbsp; | &nbsp; | &nbsp; | &nbsp; | &nbsp; |\n",
    "| **C** | u | &nbsp; | &nbsp; | &nbsp; | &nbsp; | &nbsp; | &nbsp; | &nbsp; | &nbsp; | &nbsp; |\n",
    "| **C** | u | &nbsp; | &nbsp; | &nbsp; | &nbsp; | &nbsp; | &nbsp; | &nbsp; | &nbsp; | &nbsp; |\n",
    "| **T** | u | &nbsp; | &nbsp; | &nbsp; | &nbsp; | &nbsp; | &nbsp; | &nbsp; | &nbsp; | &nbsp; |\n",
    "| **G** | u | &nbsp; | &nbsp; | &nbsp; | &nbsp; | &nbsp; | &nbsp; | &nbsp; | &nbsp; | &nbsp; |\n",
    "| **A** | u | &nbsp; | &nbsp; | &nbsp; | &nbsp; | &nbsp; | &nbsp; | &nbsp; | &nbsp; | &nbsp; ||\n",
    "\n",
    "\n",
    "Il faut ensuite remplir la matrice $M$ en suivant la formule $M_{ij} = \\max(M_{i-1j-1} + s(A_i, B_j), M_{ij-1} + s(A_i, gap), M_{i-1j} + s(B_j,gap) )$ avec $i \\in {2, \\dots, n+1}$, $j \\in {2, \\dots, m+1}$ et la fonction $s$ qui calcule le score de similarité entre deux nucléotides. Pour chaque case de $T$ on remplie par :\n",
    "- 'd' (*diagonal*) si $M_{ij}$ a été calculé en utilisant la diagonale $M_{i-1j-1}$,\n",
    "- 'l' (*left*) si $M_{ij}$ a été calculé en utilisant la case de gauche $M_{ij-1}$,\n",
    "- 'u' (*up*) si $M_{ij}$ a été calculé en utilisant la case du haut $M_{i-1j}$.\n",
    "\n",
    "On obtient alors les matrices suivantes $M$ et $T$ : \n",
    "\n",
    "|   &nbsp;   | - | A | T | C | T | C | G | T | G | A |\n",
    "|   -   | - | - | - | - | - | - | - | - | - | - |\n",
    "| **-** |  0| -5|-10|-15|-20|-25|-30|-35|-40|-45|\n",
    "| **A** | -5| 10|  5|  0| -5|-10|-15|-20|-25|-30|\n",
    "| **C** |-10|  5|  7| 12|  7|  2| -3| -8|-13|-18|\n",
    "| **T** |-15|  0| 13|  8| 20| 15| 10|  5|  0| -5|\n",
    "| **C** |-20| -5|  8| 20| 15| 27| 22| 17| 12|  7|\n",
    "| **C** |-25|-10|  3| 15| 17| 22| 22| 19| 14| 11|\n",
    "| **T** |-30|-15| -2| 10| 23| 18| 22| 30| 25| 20|\n",
    "| **G** |-35|-20| -7|  5| 18| 18| 27| 25| 39| 34|\n",
    "| **A** |-40|-25|-12|  0| 13| 17| 22| 23| 34| 49|\n",
    "\n",
    "|   &nbsp;   | - | A | T | C | T | C | G | T | G | A |\n",
    "|   -   | - | - | - | - | - | - | - | - | - | - |\n",
    "| **-** | o | l | l | l | l | l | l | l | l | l |\n",
    "| **A** | u | d | l | l | l | l | l | l | l | d |\n",
    "| **C** | u | u | d | d | l | d | l | l | l | l |\n",
    "| **T** | u | u | d | l | d | l | l | d | l | l |\n",
    "| **C** | u | u | u | d | l | d | l | l | l | l |\n",
    "| **C** | u | u | u | d | d | d | d | d | l | d |\n",
    "| **T** | u | u | d | u | d | l | d | d | l | l |\n",
    "| **G** | u | u | u | u | u | d | d | u | d | l |\n",
    "| **A** | u | d | u | u | u | d | u | d | u | d |\n",
    "\n",
    "Il suffit maintenant de regarder le dernier élément $M_{nm} = 49$ pour avoir le score de l'alignement. Pour avoir l'alignement lui-même, il faut partir de $T_{nm}$ et remonter la \"trace\" jusqu'à arriver au 'o'. Un 'd' correspond à un *match* entre les deux séquences, 'l' à un *gap* dans la séquence $A$ et 'u' à un *gap* dans la séquence $B$. En revenant à l'exemple précédent on obtient la trace suivante :\n",
    "\n",
    "|   &nbsp;   | - | A | T | C | T | C | G | T | G | A |\n",
    "|   -   | - | - | - | - | - | - | - | - | - | - |\n",
    "| **-** | o | &nbsp; | &nbsp; | &nbsp; | &nbsp; | &nbsp; | &nbsp; | &nbsp; | &nbsp; | &nbsp; |\n",
    "| **A** | &nbsp; | d | l | &nbsp; | &nbsp; | &nbsp; | &nbsp; | &nbsp; | &nbsp; | &nbsp; |\n",
    "| **C** | &nbsp; | &nbsp; | &nbsp; | d | &nbsp; | &nbsp; | &nbsp; | &nbsp; | &nbsp; | &nbsp; |\n",
    "| **T** | &nbsp; | &nbsp; | &nbsp; | &nbsp; | d | &nbsp; | &nbsp; | &nbsp; | &nbsp; | &nbsp; |\n",
    "| **C** | &nbsp; | &nbsp; | &nbsp; | &nbsp; | &nbsp; | d | &nbsp; | &nbsp; | &nbsp; | &nbsp; |\n",
    "| **C** | &nbsp; | &nbsp; | &nbsp; | &nbsp; | &nbsp; | &nbsp; | d | &nbsp; | &nbsp; | &nbsp; |\n",
    "| **T** | &nbsp; | &nbsp; | &nbsp; | &nbsp; | &nbsp; | &nbsp; | &nbsp; | d | &nbsp; | &nbsp; |\n",
    "| **G** | &nbsp; | &nbsp; | &nbsp; | &nbsp; | &nbsp; | &nbsp; | &nbsp; | &nbsp; | d | &nbsp; |\n",
    "| **A** | &nbsp; | &nbsp; | &nbsp; | &nbsp; | &nbsp; | &nbsp; | &nbsp; | &nbsp; | &nbsp; | d |\n",
    "\n",
    "Elle correspond à l'alignement :\n",
    "```\n",
    "A-CTCCTGA\n",
    "ATCTCGTGA\n",
    "```\n",
    "\n",
    "**Exercice 3 :** Implémenter l'algorithme de Needlman et Wunsch. Il prendra en paramètre deux séquences et une matrice de similarité et retournera leur alignement. Tester le avec les séquences \"ACTCCTGA\" et \"ATCTCGTGA\"."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# À remplir"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "----\n",
    "## Matrice de distance\n",
    "\n",
    "Dans le cas de séquences très proches, on estime que la distance évolutive réelle entre les séquences est proche de la p-distance qui est simplement le nombre de substitution dans l'alignement sur le nombre total de nucléotide. Pour simplifier, on ignore les positions alignées à des gaps. On applique ensuite la correction de Jukes-Cantor afin de prendre en compte le phénomène de saturation (un même site peut muter plusieurs fois au cours du temps). Sa formule est $-(\\frac{3}{4})\\ln(1-(\\frac{4}{3})\\times \\textit{p-distance})$.\n",
    "\n",
    "**Exercice 4 :** Implémenter la fonction retournant la matrice de distance à partir d'un dictionnaire de séquences. "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# À remplir"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "------\n",
    "## Construction d'un arbre avec UPGMA\n",
    "\n",
    "Grâce aux mesures de distances entre les séquences, on peut maintenant de construire l'arbre phylogénétique des globines. Vous allez devoir pour cela implémenter l'algorithme UPGMA (*unweighted pair group method with arithmetic mean*) qui, malgré son nom compliqué, est l'une des méthodes les plus simples pour la construction d'arbre.\n",
    "\n",
    "### Le format Newick\n",
    "\n",
    "Le format Newick est l'un des formats utilisé en phylogénie pour représenter un arbre sous la forme d'une chaine de caractère. Le principe est simple, les groupes ayant la même racine sont écrit entre parenthèses et séparés par des virgules. Un groupe peut être soit une feuille de l'arbre (dans notre cas une séquence), soit un autre groupe. La longueur de la branche de chaque groupe est écrite après un double point et l'arbre est terminé par un point virgule. Pour afficher l'arbre on peut utiliser les fonction du package ETE : "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from ete3 import Tree, TreeStyle\n",
    "\n",
    "newick_tree = '((A:2,(B:2,C:3):5):5,D:4);'\n",
    "t = Tree(newick_tree)\n",
    "ts = TreeStyle()\n",
    "ts.show_branch_length = True\n",
    "t.render('%%inline', w=183, units='mm', tree_style=ts)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**Exercice 5 :** Reécriver l'arbre suivant au format Newick puis afficher-le. Les nombres correspondent aux longueurs des branches :\n",
    "![](tree.png)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# À remplir"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**Exercice 6 :** Expliquer la relation de parenté entre $A$, $B$ et $C$ ? Qu'est ce qui pourrait expliquer ce type d'embranchement dans un arbre ? Donner une réponse détaillée."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Réponse : "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### UPGMA\n",
    "\n",
    "L'algorithme UPGMA se base sur la matrice de distance entre les séquences. À chaque itération, les séquences avec la distance la plus faible sont regroupées puis une nouvelle matrice de distance est calculée avec le nouveau groupe. Cette étape est répétée jusqu'à n'avoir plus qu'un seul groupe. Par exemple, avec la matrice de distance entre les séquences $A$, $B$, $C$ et $D$ suivante :\n",
    "\n",
    "|   &nbsp;   | A | B | C | D |\n",
    "|   -   | - | - | - | - |\n",
    "| **A** | &nbsp; | &nbsp; | &nbsp; | &nbsp; |\n",
    "| **B** | 4 | &nbsp; | &nbsp; | &nbsp; |\n",
    "| **C** | 8 | 8 | &nbsp; | &nbsp; |\n",
    "| **D** | 2 | 4 | 8 | &nbsp; |\n",
    "\n",
    "Les séquences $A$ et $D$ sont les plus proches ($distance(A,D)=2$). On les regroupe et on met à jour la matrice :\n",
    "\n",
    "|   &nbsp;   | (A, D) | B | C |\n",
    "|   -   | - | - | - |\n",
    "| **(A, D)** | &nbsp; | &nbsp; | &nbsp; |\n",
    "| **B** | 4 | &nbsp; | &nbsp; |\n",
    "| **C** | 8 | 8 | &nbsp; | &nbsp; |\n",
    "\n",
    "On regroupe maintenant $(A,D)$ et $B$ ($distance((A,D),B) = 4$) :\n",
    "\n",
    "|   &nbsp;   | ((A, D), B) | C |\n",
    "|   -   | - | - |\n",
    "| **((A, D), B)** | &nbsp; | &nbsp; |\n",
    "| **C** | 8 | &nbsp; |\n",
    "\n",
    "Important : les nouvelles distances sont calculées en moyennant les distances entre les membres du nouveau groupe et des groupes non modifiés pondéré par le nombre d'UTOs dans chaque groupe. Avec $i$ et $j$ les deux membres du groupe nouvellement formé et k les groupes restant : $d_{ij,k} = \\frac{n_id_{ik}}{n_i + n_j}+ \\frac{n_jd_{jk}}{n_i + n_j}$. Par exemple avec la distance entre $((A, D), B)$ et $C$:\n",
    "\n",
    "$distance(((A, D), B), C) = (distance((A, D), C)*2 + distance(B, C)) \\mathbin{/} 3 = (8*2 + 8) \\mathbin{/} 3 = 8 $.\n",
    "\n",
    "L'arbre final écrit dans le format Newick est : $((A, D), B), C);$ \n",
    "\n",
    "Et avec les distances : $((A:1, D:1):1, B:2):2, C:4);$ \n",
    "\n",
    "**Exercice 7 :** Implémenter une version d'UPGMA qui calcule l'arbre au format Newick **avec les distances** puis appliquer votre algorithme aux données. "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# À remplir"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**Exercice 8 :** Quelles sont les hypothèses faites par UPGMA ? Semblent-elles respectées dans le cas présent ?"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Réponse : "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "----\n",
    "## Enracinement de l'arbre\n",
    "\n",
    "Après avoir utilisé UPGMA pour réaliser votre arbre, l'enracinement s'est normalement fait au poids moyen. \n",
    "\n",
    "**Exercice 9 :** Quelle autre méthode est-il possible d'utiliser pour enraciner un arbre ? Pouvez-vous l'utilisez ici ? Si oui, afficher le nouvel arbre."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Réponse : "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# À remplir"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "----\n",
    "## Neighbor-joining\n",
    "\n",
    "Le neighbor-joining est un autre algorithme permettant de calculer un arbre phylogénique à partir d'une matrice de distance. Il a l'avantage de faire moins d'hypothèse qu'UPGMA sur les données (elles ne sont plus forcément ultramétrique) et il donne donc de meilleurs arbres dans presque tout les cas. Vous trouverez un example d'application de cet algorithme [ici](http://www.evolution-textbook.org/content/free/tables/Ch_27/T11_EVOW_Ch27.pdf).\n",
    "\n",
    "**Exercice 10 :** Implémenter l'algorithme du neighbor-joining, appliquer-le aux données puis enraciner l'arbre."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# À remplir"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "----\n",
    "## Conclusion\n",
    "\n",
    "**Exercice 11 :** Quelles sont vos conclusions par rapport à l'arbre phylogénique de _smilodon_, _homotherium_ et _M. trumani_ ? Comparer les deux méthodes. Comment expliquer les caractéristiques morphologiques similaires entre ces trois espèces ? Une réponse détaillée est attendue."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "collapsed": true
   },
   "source": [
    "Réponse :"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.6.8"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
