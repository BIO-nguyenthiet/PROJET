from ete3 import Tree, TreeStyle
import numpy as np
import multiprocessing
import itertools

# Input : filename -> the name of the file
# Output : Dictionnary with keys is "<specie_name> DNA" and value is the DNA code
# The file must be of this format:
# <specie_name1> DNA
# DNACODE
#
# <specie_name2> DNA
# DNACODE

debug = False


def import_fasta(filename):
    file = open(filename)
    lines = file.readlines()
    last = len(lines) - 1

    # We use a finite state automata
    # 0 : reading the specie name
    # 1 : reading it's dna code
    state = 0

    # We store the result in res
    res = {}
    current_key = ""
    current_val = ""
    curr_line = 0

    # Read lines
    for line in lines:
        # Read the specie name
        if state == 0:
            # Reset the current key and values
            current_key = ""
            current_val = ""

            # Fetch the line without ">""
            tmp = line[1:]

            # Fetch the name of the species without DNA at the end
            current_key = " ".join(tmp.split(" ")[:-1])

            # Switch to read DNA
            state = 1

        # Read the DNA code
        elif state == 1:

            # Fill value of dna
            if line != "\n" and curr_line != last:
            # if line != "\n":
                current_val += line.rstrip()
            # End of dna, switch to read specie name
            else:
                state = 0
                res[current_key] = current_val
        curr_line += 1
    file.close()
    return res


class SimilarityMatrix:
    def __init__(self, filename):
        with open(filename) as f:
            self.letters = f.readline().split()
            self.values = np.loadtxt(
                filename, skiprows=1, usecols=range(1, len(self.letters) + 1))

    def score(self, letter1, letter2):  # return the similarity score between letter1 and letter2
        return self.values[self.letters.index(letter1)][self.letters.index(letter2)]

# Compute the score(difference) between two sequences


def score_sequence(seq1, seq2):

    # In case two sequence are not same length, no overflow
    length = min(len(seq1), len(seq2))

    # The final result
    sum = 0
    similarity_matrix = SimilarityMatrix('dna_matrix')
    for i in range(length):
        sum += similarity_matrix.score(seq1[i], seq2[i])
    return sum


def p_distance(seq1, seq2):

    length = min(len(seq1), len(seq2))
    max_length = max(len(seq1), len(seq2))

    sum = 0
    for i in range(length):
        if (seq1[i] != seq2[i] and seq1[i] != "-" and seq2[i] != "-"):
            sum += 1

    return sum/max_length


def jukes_cantor(x):
    return -(3/4) * np.log(1-(4/3)*x)

# Distance matrix


def distance_matrix(seq_dict, similarity_matrix):
    # Longueur
    n = len(seq_dict)

    # Tableau d'ADN
    tab = np.array([value for key, value in seq_dict.items()])

    # Result matrix
    res = np.full([n, n], np.nan)

    # Multithread job list
    jobs = []
    manager = multiprocessing.Manager()
    return_dict = manager.dict()
    for i in range(n):
        def f(i, return_dict):
            for j in range(i):
                align1, align2 = Needleman_Wunsch(
                    tab[i], tab[j], similarity_matrix)
                tmp = jukes_cantor(p_distance(align1, align2))
                return_dict[str(i) + '#' + str(j)] = tmp
        p = multiprocessing.Process(target=f, args=(i, return_dict))
        jobs.append(p)
        p.start()

    # Wait for all job to finish
    for p in jobs:
        p.join()

    # Fill the matrix
    for key in return_dict:
        tmp = key.split('#')
        i = int(tmp[0])
        j = int(tmp[1])
        res[i][j] = return_dict[key]

    return res


# Algorithme of Needleman_Wunsch
# Input : SequenceA => string
#         SequenceB => string
#         similarity_matrix => instance of SimilarityMatrix class
# Output : two sequence which are aligned
def Needleman_Wunsch(sequenceA, sequenceB, similarity_matrix):
    # Init the two matrix
    score_matrix = np.zeros(shape=(len(sequenceA)+1, len(sequenceB)+1))
    traceback_matrix = np.full((len(sequenceA)+1, len(sequenceB)+1), '_')
    for j in range(len(sequenceB)+1):
        score_matrix[0, j] = j*(-5)
        traceback_matrix[0, j] = 'l'
    for i in range(len(sequenceA)+1):
        score_matrix[i, 0] = i*(-5)
        traceback_matrix[i, 0] = 'u'
    traceback_matrix[0, 0] = 'o'
    # compute the two matrix
    for i in range(1, len(sequenceA)+1):
        for j in range(1, len(sequenceB)+1):
            letter1 = sequenceA[i-1]
            letter2 = sequenceB[j-1]
            m_0 = score_matrix[i-1, j-1] + \
                similarity_matrix.score(letter1, letter2)
            m_1 = score_matrix[i, j-1] + similarity_matrix.score(letter1, '-')
            m_2 = score_matrix[i-1, j] + similarity_matrix.score(letter2, '-')
            m_ij = np.max([m_0, m_1, m_2])
            score_matrix[i, j] = m_ij
            if m_ij == m_0:
                traceback_matrix[i, j] = 'd'
            elif m_ij == m_1:
                traceback_matrix[i, j] = 'l'
            else:
                traceback_matrix[i, j] = 'u'
    # create the two sequence aligned
    i = len(sequenceA)
    j = len(sequenceB)
    res1 = ""
    res2 = ""
    while(traceback_matrix[i, j] != 'o'):
        if traceback_matrix[i, j] == 'd':
            res1 = sequenceA[i-1] + res1
            res2 = sequenceB[j-1] + res2
            i -= 1
            j -= 1
        elif traceback_matrix[i, j] == 'l':
            res1 = '-' + res1
            res2 = sequenceB[j-1] + res2
            j -= 1
        else:
            res1 = sequenceA[i-1] + res1
            res2 = '-' + res2
            i -= 1
    return res1, res2


# Given a string that represents a group of species like so : (toto,tata), it will return ['toto','tata']


def string_key_to_list(s):
    tmp = s.replace('(', '#').replace(')', '#').replace(',', '#').split('#')
    # Remove the '' and the max temporary weight

    res = []

    for j in range(len(tmp)):
        if tmp[j] != '' and tmp[j][0] != ':':
            res.append(tmp[j])

    return res


def two_group_into_tree(t, dl, dr, dmax):
    left = replace_max(t[0], dl)
    right = replace_max(t[1], dr)
    return '(' + left + ',' + right + '):' + str(dmax)

# t[0] must be f and t[1] g


def new_node_u(t, df, dg):
    return '(' + t[0] + ':' + str(df) + ',' + t[1] + ':' + str(dg) + ')'


def replace_max(old_s, new_max):
    l = old_s.split(':')
    if len(l) <= 1:
        return old_s + ':' + str(new_max)
    else:
        l[-1] = str(new_max)
        s = ":"
        return s.join(l)


def print_dict(t):
    for key in t:
        print(key + " : " + str(t[key]))


def upgma(seq_dict, dist_mat):
    # Number of taxa
    n = len(seq_dict)

    # Array of taxas
    tab_specie = np.array([key for key, value in seq_dict.items()])

    # Convert the distance matrix into distance hashtbl
    dist_dict = {}
    for i in range(n):
        for j in range(i):
            dist_dict[tab_specie[i] + "#" + tab_specie[j]] = dist_mat[i][j]

    # Debug
    if debug:
        n = 6
        tab_specie = ['a', 'b', 'c', 'd', 'e', 'f', 'g']
        dist_dict = {
            'a#b': 19,
            'a#c': 27,
            'a#d': 8,
            'a#e': 33,
            'a#f': 18,
            'a#g': 13,
            'b#c': 31,
            'b#d': 18,
            'b#e': 36,
            'b#f': 6,
            'b#g': 13,
            'c#d': 26,
            'c#e': 41,
            'c#f': 32,
            'c#g': 29,
            'd#e': 31,
            'd#f': 17,
            'd#g': 14,
            'e#f': 35,
            'e#g': 1,
            'f#g': 12
        }

    # The dict that keeps tracks of which specie is in the matrix
    specie_dict = {}
    for x in tab_specie:
        specie_dict[x] = True

    # The computed distance matrix
    current_dist_dict = dist_dict.copy()

    # Loop while there is more than two cells in the table
    while len(current_dist_dict) >= 1:
        # Rebuild the dictionnary at each iteration
        tmp = {}

        # Min couple represent the couple of group of taxa that have the smallest
        # distance
        min_couple = min(current_dist_dict, key=current_dist_dict.get)

        # Convert the key into a list of key, because min_cuouple is
        # equal to something similar to 'a#b' -> ['a','b']
        min_couple_list = min_couple.split('#')

        # Get the distance
        min_couple_value = current_dist_dict[min_couple]

        # Fetch the depth of the left and right tree from the min couple
        try:
            length_left = float(min_couple_list[0].split(':')[-1])
        except ValueError:
            length_left = 0

        try:
            length_right = float(min_couple_list[1].split(':')[-1])
        except ValueError:
            length_right = 0

        # Compute the distance of the min couple to the new root
        new_dist_left = (min_couple_value / 2) - length_left
        new_dist_right = (min_couple_value / 2) - length_right
        length_new = min_couple_value / 2

        # Create the new key that is a newick tree
        min_couple_key = two_group_into_tree(
            min_couple_list, new_dist_left, new_dist_right, length_new)

        # If we only have 2 groups, we can return
        if len(current_dist_dict) == 1:
            l = min_couple_key.split(":")
            res = ":".join(l[:-1]) + ";"
            return res

        # Remove the two from the know specie
        specie_dict.pop(min_couple_list[0])
        specie_dict.pop(min_couple_list[1])

        # How many item in the first cluster
        n1 = len(string_key_to_list(min_couple_list[0]))
        # How many item in the second cluster
        n2 = len(string_key_to_list(min_couple_list[1]))
        # Sum of the two before
        nbis = n1 + n2

        # Functios that compute the distance to the newly formed
        # tree to the rest of the taxas
        def f1(a): return (n1 * a)/(nbis)

        def f2(a): return (n2 * a)/(nbis)

        # The distance between the newly formed tree and the rest
        for specie in specie_dict:
            try:
                dik = current_dist_dict[min_couple_list[0]+"#"+specie]
            except:
                dik = current_dist_dict[specie+"#"+min_couple_list[0]]

            try:
                djk = current_dist_dict[min_couple_list[1]+"#"+specie]
            except:
                djk = current_dist_dict[specie+"#"+min_couple_list[1]]

            tmp[min_couple_key + '#' + specie] = f1(dik) + f2(djk)

        # Recreate the matrix for the rest of the taxa that wasn't changed
        for k1, k2 in itertools.combinations(specie_dict, 2):
            key = k1 + '#' + k2
            try:
                tmp[key] = current_dist_dict[key]
            except:
                tmp[key] = current_dist_dict[k2 + '#' + k1]

        # Add the newly formed tree into the dict that keeps track of whick specie is in the matrix
        specie_dict[min_couple_key] = True

        # Update the matrix/dict of distances
        current_dist_dict = tmp


def neighbor_joining(seq_dict, dist_mat):

    # Debug
    if debug:
        n = 6
        tab_specie = ['a', 'b', 'c', 'd', 'e']
        dist_dict = {
            "a#b": 5,
            "a#c": 9,
            "a#d": 9,
            "a#e": 8,
            "b#c": 10,
            "b#d": 10,
            "b#e": 9,
            "c#d": 8,
            "c#e": 7,
            "d#e": 3
        }
    else:
        # Number of taxa
        n = len(seq_dict)

        # Array of taxas
        tab_specie = np.array([key for key, value in seq_dict.items()])

        # Convert the distance matrix into distance hashtbl
        dist_dict = {}
        for i in range(n):
            for j in range(i):
                dist_dict[tab_specie[i] + "#" + tab_specie[j]] = dist_mat[i][j]

    # The dict that keeps tracks of which specie is in the matrix
    specie_dict = {}
    for x in tab_specie:
        specie_dict[x] = True

    # The computed distance matrix
    current_dist_dict = dist_dict.copy()

    # Loop while there is more than two cells in the table
    while len(current_dist_dict) >= 1:
        # The matrix Q
        Q = dict()

        # Rebuild the dictionnary at each iteration
        tmp = {}

        # Build the Q matrix
        specie_list = list(specie_dict.keys())
        for i in range(len(specie_list)):
            for j in range(i):
                try:
                    tmp_q = (len(specie_list) - 2) * \
                        current_dist_dict[specie_list[i]+"#"+specie_list[j]]
                except:
                    tmp_q = (len(specie_list) - 2) * \
                        current_dist_dict[specie_list[j]+"#"+specie_list[i]]
                sum1 = 0
                sum2 = 0
                for k in range(len(specie_list)):
                    if i != k:
                        try:
                            sum1 += current_dist_dict[specie_list[i] +
                                                      "#"+specie_list[k]]
                        except:
                            sum1 += current_dist_dict[specie_list[k] +
                                                      "#"+specie_list[i]]
                    if j != k:
                        try:
                            sum2 += current_dist_dict[specie_list[j] +
                                                      "#"+specie_list[k]]
                        except:
                            sum2 += current_dist_dict[specie_list[k] +
                                                      "#"+specie_list[j]]
                Q[specie_list[i]+"#"+specie_list[j]] = tmp_q - sum1 - sum2

        # Min couple represent the couple of group of taxa that have the smallest
        # distance, it's the new node u
        min_couple = min(Q, key=Q.get)

        # Convert the key into a list of key, because min_cuouple is
        # equal to something similar to 'a#b' -> ['a','b']
        min_couple_list = min_couple.split('#')

        # The other key that works
        min_couple_bis = min_couple_list[1] + '#' + min_couple_list[0]

        # Get the distance
        try:
            min_couple_value = current_dist_dict[min_couple]
        except:
            min_couple_value = current_dist_dict[min_couple_bis]

        # If we only have 2 groups, we can return
        if len(current_dist_dict) == 1:
            w = min_couple_value/2
            return '(' + min_couple_list[0] + ':' + str(w) + ',' + min_couple_list[1] + ':' + str(w) +');'

       
        # Compute the distance of the min couple to the new root
        sum_f_k = 0
        sum_g_k = 0
        f = min_couple_list[0]
        g = min_couple_list[1]
        for k in range(len(specie_list)):
            if f != specie_list[k]:
                try:
                    sum_f_k += current_dist_dict[f+"#"+specie_list[k]]
                except:
                    sum_f_k += current_dist_dict[specie_list[k]+"#"+f]
            if g != specie_list[k]:
                try:
                    sum_g_k += current_dist_dict[g+"#"+specie_list[k]]
                except:
                    sum_g_k += current_dist_dict[specie_list[k]+"#"+g]

        new_dist_f = (min_couple_value / 2) + (sum_f_k -
                                               sum_g_k) / (2 * ((len(specie_dict) - 2)))
        try:
            new_dist_g = current_dist_dict[min_couple] - new_dist_f
        except:
            new_dist_g = current_dist_dict[min_couple_bis] - new_dist_f

        # Create the new node
        min_couple_key = new_node_u(min_couple_list, new_dist_f, new_dist_g)

        # Remove the two from the known specie
        specie_dict.pop(min_couple_list[0])
        specie_dict.pop(min_couple_list[1])

        # The distance between the newly node and the rest
        for specie in specie_dict:
            try:
                dfk = current_dist_dict[min_couple_list[0]+"#"+specie]
            except:
                dfk = current_dist_dict[specie+"#"+min_couple_list[0]]
            try:
                dgk = current_dist_dict[min_couple_list[1]+"#"+specie]
            except:
                dgk = current_dist_dict[specie+"#"+min_couple_list[1]]

            tmp[min_couple_key + '#' + specie] = 1 / \
                2 * (dfk + dgk - min_couple_value)

        # Recreate the matrix for the rest of the taxa that wasn't changed
        specie_list = list(specie_dict.keys())
        for i in range(len(specie_list)):
            for j in range(i):
                key = specie_list[i] + '#' + specie_list[j]
                try:
                    tmp[key] = current_dist_dict[key]
                except:
                    key_bis = specie_list[j] + '#' + specie_list[i]
                    tmp[key] = current_dist_dict[key_bis]

        # Add the newly formed tree into the dict that keeps track of whick specie is in the matrix
        specie_dict[min_couple_key] = True

        # Update the matrix/dict of distances
        current_dist_dict = tmp


if __name__ == "__main__":
    similarity_matrix = SimilarityMatrix('dna_matrix')
    dict_dna = import_fasta("cat_dna.fasta")
    dist_mat = distance_matrix(dict_dna, similarity_matrix)

    # Q5
    # newick_tree = '((A:10,B:8,C:7):5,(D:9,(E:5,F:5):2):2):0;'
    # t = Tree(newick_tree)
    # ts = TreeStyle()
    # ts.show_branch_length = True
    # t.render(file_name='arbre.png', w=183, units='mm', tree_style=ts)

    # Q6
    # A B et C ont le même parent. Cette embranchement peut s'expliquer
    # par deux mutations qui ont eu lieu en même temps

    # Q7
    print("-----------------------------------------------------------------------------------------------------")
    print("debut UPGMA")
    machin = upgma(dict_dna, dist_mat)
    tbis = Tree(machin)
    tsbis = TreeStyle()
    tsbis.show_branch_length = True
    tbis.render(file_name='upgma.png', h=183, units='mm', tree_style=tsbis)
    print("Fin UPGMA")
    # Q8
    # On fait l'hypothese d'horloge moléculaire. On obtient un arbre ultramétrique ce qui confirme l'hypothése.

    # Q9
    # On peut faire l'enracinement avec groupe extérieur, mais on n'a pas fait d'analyse d'un groupe de séquence
    # homologue à celles analysées. De plus on pourrait ne pas garder la forme ultramétrique requise pour UPGMA

    # print("------------------------------------------------------------------------------------------------------")
    print("Debut Neighbor joining")
    # Q10
    dict_taxa = matrix_to_dict(dist_mat, dict_dna)
    arbre = neighbor_joining(dict_dna, dist_mat)
    t = Tree(arbre)
    ts = TreeStyle()
    ts.show_branch_length = True
    t.render(file_name='neighbour_joining.png',
             w=183, units='mm', tree_style=ts)
    print("Fin Neighbor joining")

